# Travis-ci Dark Grey

![](images/screenshot.png)

A dark mode for tracis-ci's jobs pages.

You can download the styling [here](https://userstyles.org/styles/169366/travis-ci-dark-grey)
